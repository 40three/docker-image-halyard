Halyard Image and Deployment for Kubernetes
===========================================

The included halyard.yaml can be used with `kubectl apply` to deploy a halyard
instance to a kubernetes cluster. /home/spinnaker will be set up as a persistent
volume to persist configuration data.

The included Dockerfile creates a halyard-deployment image based on the official
Spinnaker docker image. It just adds some tools.

The built image is automatically tagged as "stable" and with the same name as the git 
tag, if any. To build a new version of the image, simply create a new tag and push it to
github.
