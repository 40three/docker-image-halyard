FROM  us-docker.pkg.dev/spinnaker-community/docker/halyard:stable

USER root

RUN rm -rf /var/cache/apk \
    && mkdir /var/cache/apk \
    && apk update \ 
    && apk add vim less \
    && rm -rf /var/cache/apk/*
    
USER spinnaker

RUN echo "source <(kubectl completion bash)" >> /home/spinnaker/.bashrc
RUN echo "source <(hal --print-bash-completion)" >> /home/spinnaker/.bashrc
